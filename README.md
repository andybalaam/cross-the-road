# cross-the-road

A simple game of crossing a road written in Elm.

Play it at [andybalaam.gitlab.io/cross-the-road](https://andybalaam.gitlab.io/cross-the-road/).

Copyright 2018 Andy Balaam.

Released under AGPLv3.  See [LICENSE](LICENSE) for information.
