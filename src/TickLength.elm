module TickLength exposing (tickLength)


tickLength : Float
tickLength = 50  -- milliseconds per frame
